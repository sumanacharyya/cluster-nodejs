const express = require("express");

const app = express();
const PORT = 8080;

app.get("/", (req, res) => {
  return res.json({ message: `Welcome to Cluster server ${process.pid}` });
});

app.listen(PORT, (req, res) => {
  console.log(`App listening at port -> http://localhost:${PORT}`);
});
