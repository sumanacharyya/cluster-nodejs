const cluster = require("node:cluster");
const os = require("os");

const express = require("express");

const totalCpus = os.cpus().length;

if (cluster.isPrimary) {
  // Fork workers.
  for (let i = 0; i < totalCpus; i++) {
    cluster.fork();
  }

  cluster.on("exit", (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  const app = express();
  const PORT = 8080;

  app.get("/", (req, res) => {
    return res.json({ message: `Welcome to Cluster server ${process.pid}` });
  });

  app.listen(PORT, (req, res) => {
    console.log(`App listening at port -> http://localhost:${PORT}`);
  });
}
